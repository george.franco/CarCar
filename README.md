# CarCar
Team:

* Person 1 - Alex Kagawa: sales microservice
* Person 2 - George Franco: services microservice


## The assignment
This project tests your ability to use Django to create RESTful APIs in microservices. It also tests your ability to use React to create a front-end application that uses your RESTful APIs. This project is a paired project.

The project is CarCar, an application for managing aspects of an automobile dealership, specifically its inventory, service center, and sales.

The starter application comes with a fully-functioning scaffold of microservices, a front-end application, and a database. The services encloed by backticks in the following list are the ones that you and your teammate will implement:

* `Inventory API`: provides `Manufacturer`, `VehicleModel`, and `Automobile` RESTful API endpoints but needs a front-end
* Database: the PostgreSQL database that will hold the data of all of the microservices
* `React`: the React-based front-end application where both you and your teammate will write the components to interact with your services **as well as the Inventory service
* `Service API`: the RESTful API to handle automobile service appointments
* `Service Poller`: a poller to use to integrate with other services
* `Sales API`: the RESTful API to handle sales information
* `Sales Poller`: a poller to use to integrate with other services


## Service microservice
I will create three models: AutomobileVO, appointments, and technicians. The automobileVO should exist in both the sales and service microservices. This is because both microservices will need that data that comes from the inventory app. For my models, the only ForeignKey is found in appointments. It is a FK that ties Technicians to a one-to-many relationship. The reason for this is because one technician can be assigned to an appointment, but a singular technician can be signed up for multiple appointments.

The Appointment and technician models will be used as CRUD apps to that allow us to create, read, update, and delete data. After this I will implement the poller so that my model views can connect to the api data source. The poller.py file will poll the automobiles api so that the automobile data gets populated. As mentioned, the automobile data and models comes from the inventory app. 

Finally we will implement the various front end forms with React. These service forms allow for users to schedule appointments, view active appointments (and deactivate those that need to be), it also allows for users to search appointment history and filter it by the vin. Lastly, with React users can create new Technicians by entering their name and unique employee ID.


## Sales microservice
Project:

Within the sales microservice, I initially implemented three models: SalesPerson, Customer, and Sale.  The Sales model included two ForeignKeys: one to SalesPerson, and one to Customer.  The Sale model also included a OneToOne relationship with the Automobile model in the inventory microservice.  To leverage the Automobile data in the inventory microservices' database, I created an AutomobileVO model, and implemented a pollar, get_automobiles(), to make a call to the Automobile database every 10 seconds.  This synchronized the data between the Automobile and the AutomobileVO models.

Next, I implemented my CRUD views and URL patterns for the SalesPerson, Customer, and Sale models, and added encoders for each.  Once each API was tested, I built out screens in React for the sales microservice, which included: (1.) a list view of CarCar's sales history, which was filterable by salesperson, (2.) a list view of all prior automobile sales, and (3.) an intake form, which enabled users to create new sale instances.  Beyond my microservice, I also built six new screens in React for the inventory microservice to display or manipulate those models, including: (1.) a list of manufacturers, (2.) a form to create new manufacturers, (3.) a list of vehicle models, (4.) a form to create new vehicle models, (5.) a list of automobiles, and (6.) a form to create new automobiles.

Stretch Goals:

Given how the models were architected in the inventory microservice, I implemented two additional value object models in the sales microservice: a VehicleModelVO and a ManufacturerVO, in order to include data points like the vehicle model name (e.g. Tacoma), the manufacturer name (e.g. Toyota), and the vehicle model picture URL in my microservice's React screens.  Once the value objects were created, I then had to create pollars for each: get_vehicle_models() and get_manufacturers(), as well as encoders in order to call the data points I needed.  Once both microservices' data was synchronized, I then updated the existing React screens that I had implemented with the newly available data.

Secondarily, I decided the app could benefit from being able to store more than one picture per vehicle model, so I implemented an Image model in the inventory microservice, and related it to the VehicleModel model as a ForeignKey.  I then created an ImageVO model in the sales microservice, implemented a forth pollar to synchronize the Image and ImageVO models, created the appropriate encoders, and then implemented a Popup component in React, which displays a carousel of images for each vehicle model, which can be accessed from any of the screens highlighting either automobiles or vehicle models.  The same code used to create the carousel of images in the Popup component was then repurposed for the homepage, and lastly, I worked with my partner to standardize all styling and spacing across the website.