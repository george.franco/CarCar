# Generated by Django 4.0.3 on 2022-09-14 22:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventory_rest', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('automobile', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='images', to='inventory_rest.automobile')),
            ],
        ),
    ]
