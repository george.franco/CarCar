import React from 'react';
import { Link } from 'react-router-dom';


class ManufacturerForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: ""
        }
    }


    handleChange = event => {
        const value = event.target.value;
        this.setState({
            name: value
        });
    }


    handleSubmit = async event => {
        event.preventDefault();
        const data = {...this.state};

        const resp = await fetch ('http://localhost:8100/api/manufacturers/', {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        if (resp.ok) {
            const newManufacturer = await resp.json();
            window.location.href = "/manufacturers";
        }
    }


    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <br />
                    <Link to="/manufacturers" className="btn btn-primary btn-lg px-4 gap-3" style={{backgroundColor: '#308454', border: 'none'}}>Back</Link>
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Manufacturer</h1>
                        <form onSubmit={this.handleSubmit} id="create-sale-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChange} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <button className="btn btn-primary" style={{backgroundColor: '#308454', border: 'none'}}>Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default ManufacturerForm;