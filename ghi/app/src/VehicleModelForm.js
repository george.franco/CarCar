import React from 'react';
import { Link } from 'react-router-dom';


class VehicleModelForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            pictureUrl: "",
            manufacturer: "",
            manufacturers: [],
        }
    }


    async componentDidMount() {
        const resp = await fetch('http://localhost:8100/api/manufacturers/');
        if (resp.ok) {
            const data = await resp.json();
            this.setState({
                manufacturers: data.manufacturers
            });
        }
    }


    handleChange = event => {
        const {name, value} = event.target;
        this.setState({
            [name]: value
        });
    }


    handleSubmit = async event => {
        event.preventDefault();
        const data = {...this.state};
        data.picture_url = data.pictureUrl;
        data.manufacturer_id = data.manufacturer
        delete data.pictureUrl;
        delete data.manufacturers;

        const resp = await fetch ('http://localhost:8100/api/models/', {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        if (resp.ok) {
            const newVehicleModel = await resp.json();
            window.location.href = "/vehicle-models";
        }
    }


    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <br />
                    <Link to="/vehicle-models" className="btn btn-primary btn-lg px-4 gap-3" style={{backgroundColor: '#308454', border: 'none'}}>Back</Link>
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Vehicle Model</h1>
                        <form onSubmit={this.handleSubmit} id="create-vehicle-model-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChange} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChange} value={this.state.pictureUrl} placeholder="pictureUrl" required type="text" name="pictureUrl" id="pictureUrl" className="form-control"/>
                            <label htmlFor="pictureUrl">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleChange} value={this.state.manufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                                <option value="">Choose a Manufacturer</option>
                                {
                                    this.state.manufacturers.map(manufacturer => {
                                        return <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                    })
                                }
                            </select>
                        </div>
                        <button className="btn btn-primary" style={{backgroundColor: '#308454', border: 'none'}}>Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default VehicleModelForm;