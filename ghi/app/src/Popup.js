import React from 'react';
import { Carousel } from 'react-responsive-carousel';
import styles from 'react-responsive-carousel/lib/styles/carousel.min.css';

class Popup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            manufacturerName: "",
            modelName: "",
            images: [],
        }
    }
    
    
    async componentDidMount() {
        if (this.props.vin !== "") {
            const automobileResp = await fetch(`http://localhost:8100/api/automobiles/${this.props.vin}/`);
            if (automobileResp.ok) {
                const automobileData = await automobileResp.json();
                this.setState({
                    manufacturerName: automobileData.model.manufacturer.name,
                    modelName: automobileData.model.name,
                });
            } else {
                console.log("There was an error fetching automobile details: ", imageResp);
            }
        } else {
            this.setState({
                manufacturerName: this.props.model.manufacturer.name,
                modelName: this.props.model.name,
            });
        }
        

        const imageResp = await fetch('http://localhost:8100/api/images/');
        if (imageResp.ok) {
            const imageData = await imageResp.json();
            const imageList = [];
            for (const image of imageData.images) {
                if (image.model.name === this.state.modelName) {
                    imageList.push(image)
                }
            }
            this.setState({
                images: imageList
            })
        } else {
            console.log("There was an error fetching list of images: ", imageResp);
        }
    }
    
    
    render() {
        return(
            <div className="popup-box">
                <div className="box">
                    <span className="close-icon" onClick={this.props.toggleClose}>x</span>
                    <h2>{this.state.manufacturerName} {this.state.modelName}</h2>
                    <br />
                    <Carousel>
                        {this.state.images.map(image => {
                            return (
                                <div key={image.id}>
                                    <img src={image.picture_url} className="popup-image"/>
                                </div>
                            );
                        })}
                    </Carousel>
                    <div></div>
                </div>
            </div>
        );
    }
}

export default Popup;