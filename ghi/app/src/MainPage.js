import { Carousel } from 'react-responsive-carousel';
import { Link } from 'react-router-dom';

function MainPage() {
    const image1Href = "https://www.motorbiscuit.com/wp-content/uploads/2021/06/Tesla-Model-3-4.jpg";

    const image2Href = "https://di-uploads-pod10.dealerinspire.com/mercedesbenzofsmithtown/uploads/2020/11/iris.png";

    const image3Href = "https://rare-gallery.com/uploads/posts/888444-Niva-Off-Road-2123-2020-Lada-SUV-Black-White.jpg";

    const image4Href = "https://blogmedia.dealerfire.com/wp-content/uploads/sites/376/2018/05/Color-Options-for-the-2018-Nissan-Frontier-b4_o.jpg";

    const image5Href = "https://www.topgear.com/sites/default/files/images/news-article/2019/12/922ce89fcd3c73f62d4bbb410a3faa66/2020-lada-4x4-russian-spec-1.jpg";
    
    const imageList = [image1Href, image2Href, image3Href, image4Href, image5Href];
    
    
    return (
        <div className="px-4 py-5 my-5 text-center">
            <h1 className="display-5 fw-bold">CarCar</h1>
            <div className="col-lg-6 mx-auto">
                <p className="lead mb-4">
                  The premiere solution for automobile dealership management!
                </p>
                <Link to="/sales" className="btn btn-primary btn-lg px-4 gap-3" style={{backgroundColor: '#308454', border: 'none'}}>View Our Recently Sold Inventory</Link>
            </div>
            <Carousel autoPlay infiniteLoop interval={4000} transitionTime={500} controls={false} slide={true} indicatorLabels={false} showThumbs={false}>
                {imageList.map(imageHref => {
                    return (
                        <img src={imageHref} alt="image2"/>
                    );
                })}
            </Carousel>
        </div>
    );
}

export default MainPage;
