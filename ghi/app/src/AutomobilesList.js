import React from 'react';
import { Link } from 'react-router-dom';
import Popup from './Popup';


class AutomobilesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            automobiles: [],
            popupIsOpen: false,
            vin: "",
        }
    }


    async componentDidMount() {
        const automobilesResp = await fetch('http://localhost:8100/api/automobiles/');
        if (automobilesResp.ok) {
            const data = await automobilesResp.json();
            this.setState({
                automobiles: data.autos
            });
        } else {
            console.log("There was an error fetching list of automobiles: ", automobilesResp);
        }
    }

 
    togglePopup = (vin) => {
        this.setState({
            popupIsOpen: !this.state.popupIsOpen,
            vin: vin,
        });
    }
    
    
    render() {
        return(
            <React.StrictMode>
            <div>
                <br />
                <h1>Automobiles</h1>
                <br />
                <Link to="/automobiles/new" className="btn btn-primary btn-lg px-4 gap-3" style={{backgroundColor: '#308454', border: 'none'}}>Create New Automobile</Link>
                <br />
                <br />
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Model</th>
                            <th>Manufacturer</th>
                            <th>VIN</th>
                            <th>Color</th>
                            <th>Year</th>
                            <th>Image</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.automobiles.map(automobile => {
                        return (
                            <tr key={automobile.id}>
                                <td>{automobile.model.name}</td>
                                <td>{automobile.model.manufacturer.name}</td>
                                <td>{automobile.vin}</td>
                                <td>{automobile.color}</td>
                                <td>{automobile.year}</td>
                                <td>
                                    <button className="btn btn-primary" onClick={() => this.togglePopup(automobile.vin)} style={{backgroundColor: '#308454', border: 'none'}}>View Images</button>
                                    {this.state.popupIsOpen && <Popup vin={this.state.vin} toggleClose={this.togglePopup} />}
                                </td>
                            </tr>
                        );
                    })}
                    </tbody>
                </table>
                <br />
            </div>
            </React.StrictMode>
        );
    }
}

export default AutomobilesList;