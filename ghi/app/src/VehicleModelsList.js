import React from 'react';
import { Link } from 'react-router-dom';
import Popup from './Popup';


class VehicleModelsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vehicleModels: [],
            images: [],
            popupIsOpen: false,
        }
    }


    async componentDidMount() {
        const resp = await fetch('http://localhost:8100/api/models/');
        if (resp.ok) {
            const data = await resp.json();
            this.setState({
                vehicleModels: data.models
            })
        } else {
            console.log("There was an error fetching list of vehicle models: ", resp);
        }
    }


    togglePopup = (model) => {
        this.setState({
            popupIsOpen: !this.state.popupIsOpen,
            model: model,
        });
    }
    
    
    render() {
        return(
            <React.StrictMode>
            <div>
                <br />
                <h1>Vehicle Models</h1>
                <br />
                <Link to="/vehicle-models/new" className="btn btn-primary btn-lg px-4 gap-3" style={{backgroundColor: '#308454', border: 'none'}}>Create New Vehicle Model</Link>
                <br />
                <br />
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Model</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.vehicleModels.map(model => {
                        return (
                        <tr key={model.id}>
                            <td>{model.name}</td>
                            <td>{model.manufacturer.name}</td>
                            <td>
                                <div className="thumbnail-container">
                                    <img className="thumbnail" src={model.picture_url} onClick={() => this.togglePopup(model)} />
                                </div>
                                {this.state.popupIsOpen && <Popup vin="" model={this.state.model} toggleClose={this.togglePopup} />}
                            </td>
                        </tr>
                        );
                    })}
                    </tbody>
                </table>
                <br />
            </div>
            </React.StrictMode>
        );
    }
}

export default VehicleModelsList;