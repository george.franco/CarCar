import React from 'react';
import { Link } from 'react-router-dom';
import Popup from './Popup';


class SalesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sales: [],
            images: [],
            popupIsOpen: false,
        }
    }


    async componentDidMount() {
        const resp = await fetch('http://localhost:8090/api/sales/');
        if (resp.ok) {
            const data = await resp.json();
            this.setState({
                sales: data.sales
            })
        } else {
            console.log("There was an error fetching list of sales: ", resp);
        }
    }


    togglePopup = (model) => {
        this.setState({
            popupIsOpen: !this.state.popupIsOpen,
            model: model,
        });
    }
    
    
    render() {
        return(
            <React.StrictMode>
            <div>
                <br />
                <h1>Automobile Sales</h1>
                <br />
                <Link to="/sales/new" className="btn btn-primary btn-lg px-4 gap-3" style={{backgroundColor: '#308454', border: 'none'}}>Create New Sale</Link>
                <br />
                <br />
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Model</th>
                            <th>Manufacturer</th>
                            <th>Image</th>
                            <th>Sales Person</th>
                            <th>Customer</th>
                            <th>Sale Price</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.sales.map(sale => {
                        return (
                        <tr key={sale.id}>
                            <td>{sale.automobile.model.name}</td>
                            <td>{sale.automobile.model.manufacturer.name}</td>
                            <td>
                                <div className="thumbnail-container">
                                    <img className="thumbnail" src={sale.automobile.model.picture_url} onClick={() => this.togglePopup(sale.automobile.model)}/>
                                </div>
                                {this.state.popupIsOpen && <Popup vin="" model={this.state.model} toggleClose={this.togglePopup} />}
                            </td>
                            <td>{sale.sales_person.name}</td>
                            <td>{sale.customer.name}</td>
                            <td>${sale.price.toLocaleString()}</td>
                        </tr>
                        );
                    })}
                    </tbody>
                </table>
                <br />
            </div>
            </React.StrictMode>
        );
    }
}

export default SalesList;