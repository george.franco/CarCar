import React from "react";
import Moment from 'moment';

class ServiceList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      appointments: [],
    };
  }

  async componentDidMount() {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      this.setState({
        appointments: data,
      });
    }
  }

  handleChange = async (event) => {
    const value = event.target.value;
    const vinsList = [];
    if (value === "") {
      this.setState({ appointments: this.state.appointments });
      window.location.reload();
    } else {
      for (const service of this.state.appointments) {
        if (service.vin.toString() === value) {
          vinsList.push(service);
        } else {
          console.log("THIS IS NOT WORKING");
          console.log("List", vinsList);
        }
      }
      this.setState({ appointments: vinsList });
    }
  };


  render() {
    return (
      <React.StrictMode>
        <div>
          <br />
          <h1>Service History</h1>
          <br />
          <select
            onChange={this.handleChange}
            name="service"
            id="service"
            className="form-select"
          >
            <option value="">Filter by VIN</option>
            {this.state.appointments.map((appointment) => {
              return (
                <option key={appointment.vin.id} value={appointment.vin}>
                  {appointment.vin}
                </option>
              );
            })}
          </select>
          <br />
          <table className="table table-striped">
            <thead>
              <tr>
                <th>VIN</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Technician</th>
                <th>Reason for service</th>
              </tr>
            </thead>
            <tbody>
              {this.state.appointments.map((appointment) => {
                return (
                  <tr key={appointment.vin.id}>
                    <td>{appointment.vin}</td>
                    <td>{appointment.customer_name}</td>
                    <td>{Moment(appointment.time).format('MM-DD-YYYY HH:MM A')}</td>
                    <td>{appointment.technician.name}</td>
                    <td>{appointment.reason}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </React.StrictMode>
    );
  }
}

export default ServiceList;
