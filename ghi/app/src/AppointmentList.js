import React from "react";
import { Link } from "react-router-dom";
import Moment from 'moment';

class AppointmentList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      appointments: [],
    };
    this.handleRemove = this.handleRemove.bind(this);
  }

  async componentDidMount() {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      this.setState({
        appointments: data,
      });
    }
  }

  async handleRemove(id) {
    const data = { ...this.state };
    const appointmentUrl = `http://localhost:8080/api/appointments/${id}/`;
    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify({ active: false }),
    };
    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      const data = await response.json;
    //   this.setState({
    //     appointments: data,
    //   });
    //   console.log("state", this.state)
    //   console.log("appointments", this.state.appointments)
    // }
    window.location.reload();
    // const cleared = {
    //     appointments: []
    // }
    // this.setState(cleared);

  }}

  render() {
    return (
      <React.StrictMode>
        <div>
          <div>
            <br></br>
            <h1>List of appointments</h1>
          </div>
          <br />
          <div>
            <Link to="/appointments/new" className="btn btn-primary btn-lg" style={{backgroundColor: '#308454', border: 'none'}}>
              Schedule an appointment
            </Link>
          </div>
          <br />
          <table className="table table-striped">
            <thead>
              <tr>
                <th>VIN</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Technician</th>
                <th>Reason for service</th>
              </tr>
            </thead>
            <tbody>
              {this.state.appointments
                .filter((appointment) => appointment.active !== false)
                .map((appointment) => {
                  return (
                    <tr id="appointments" key={appointment.id}>
                      <td>{appointment.vin}</td>
                      <td>{appointment.customer_name}</td>
                      <td>{Moment(appointment.time).format('MM-DD-YYYY HH:MM A')}</td>
                      <td>{appointment.technician.name}</td>
                      <td>{appointment.reason}</td>
                      <td>
                        <button
                          onClick={() => this.handleRemove(appointment.id)}
                          style={{backgroundColor: '#d64161', border: 'none', color: 'white', borderRadius: '5px'}}
                        >
                          Cancel
                        </button>
                      </td>
                      <td>
                        <button
                          onClick={() => this.handleRemove(appointment.id)}
                          style={{backgroundColor: '#308454', border: 'none', color: 'white', borderRadius: '5px'}}
                        >
                          Completed
                        </button>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
      </React.StrictMode>
    );
  }
}

export default AppointmentList;
