import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-current="page" to="#">Sales</NavLink>
              <div className="dropdown-menu dropdown-menu-end">
                <NavLink className="navlink dropdown-item" to="/sales">Automobile Sales</NavLink>
                <NavLink className="navlink dropdown-item" to="/sales-person-history">Sales Person History</NavLink>
              </div>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-current="page" to="#">Services</NavLink>
              <div className="dropdown-menu dropdown-menu-end">
                <NavLink className="navlink dropdown-item" to="/appointments">Service Appointments</NavLink>
                <NavLink className="navlink dropdown-item" to="/service-history">Service History</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="navlink dropdown-item" to="/technicians/new">Add a Technician</NavLink>
              </div>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-current="page" to="#">Inventory</NavLink>
              <div className="dropdown-menu dropdown-menu-end">
                <NavLink className="navlink dropdown-item" to="/manufacturers">Manufacturers</NavLink>
                <NavLink className="navlink dropdown-item" to="/vehicle-models">Vehicle Models</NavLink>
                <NavLink className="navlink dropdown-item" to="/automobiles">Automobiles</NavLink>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
