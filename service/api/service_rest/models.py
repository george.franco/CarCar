from django.db import models
from django.urls import reverse

# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)

    def __str__(self):
        return self.vin

class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_id = models.AutoField(primary_key=True)
    
    def get_api_url(self):
        return reverse("api_list_technicians")


class Appointment(models.Model):
    customer_name = models.CharField(max_length=200)
    vin = models.CharField(max_length=17)
    vip = models.BooleanField(default=False)
    time = models.DateTimeField(blank=True, auto_now_add=True)
    technician = models.ForeignKey(Technician, related_name="technicians", on_delete=models.PROTECT)
    reason = models.TextField()
    active = models.BooleanField(default=True)

    def get_api_url(self):
        return reverse("api_list_appointments")

