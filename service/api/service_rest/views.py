from django.shortcuts import render
from .models import AutomobileVO, Technician, Appointment
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .encoders import AppointmentDetailEncoder, AppointmentListEncoder, AutomobileVODetailEncoder, TechnicianDetailEncoder, TechnicianListEncoder
# Create your views here.




@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointment = Appointment.objects.all()
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            vin = AutomobileVO.objects.get(vin=content["vin"])
            content["vin"] = vin
            content["vip"] = True
        except AutomobileVO.DoesNotExist:
                # return JsonResponse(
                #     {"message": "Invalid vin number"},
                #     status=404,
                # )
                content["vip"] = False
                vin = None
        try:
            technician = Technician.objects.get(employee_id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician"},
                status=404
            )
            
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment, encoder=AppointmentListEncoder, safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_detail_appointments(request, pk):
    if request.method == "GET": 
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse({"appointment": appointment}, encoder=AppointmentDetailEncoder, safe=False)



@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician, encoder=TechnicianListEncoder, safe=False
        )
    