from .models import AutomobileVO, Technician, Appointment
from common.json import ModelEncoder

class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["id", "vin"]

class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = ["name", "employee_id"]

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["name", "employee_id"]

class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = ["customer_name", "vin", "technician", "reason", "vip", "time", "id", "active"]
    encoders = {
        "technician": TechnicianListEncoder(),
    }


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["customer_name", "vin", "technician", "reason", "vip", "time", "id", "active"]
    encoders = {
        "technician": TechnicianListEncoder(),
    }