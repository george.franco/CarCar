# Generated by Django 4.0.3 on 2022-09-15 19:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0002_alter_appointment_vin_alter_automobilevo_vin'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='active',
            field=models.BooleanField(default=False),
        ),
    ]
