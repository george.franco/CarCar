from django.db import models
from django.urls import reverse

# Create your models here.
class ManufacturerVO(models.Model):
    name = models.CharField(max_length=100, unique=True)


class VehicleModelVO(models.Model):
    name = models.CharField(max_length=100)
    picture_url = models.URLField()
    manufacturer = models.ForeignKey(
        ManufacturerVO,
        related_name="models",
        on_delete=models.CASCADE,
        null=True,
    )


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    model = models.ForeignKey(
        VehicleModelVO,
        related_name="automobiles",
        on_delete=models.CASCADE,
        null=True,
    )


class ImageVO(models.Model):
    picture_url = models.URLField()
    model = models.ForeignKey(
        VehicleModelVO,
        related_name="images",
        on_delete=models.CASCADE,
        null=True,
    )


class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_list_sales_people")


class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone = models.CharField(max_length=200)

    def get_api_url(self):
        return reverse("api_list_customers")


class Sale(models.Model):
    automobile = models.OneToOneField(AutomobileVO, on_delete=models.CASCADE)
    sales_person = models.ForeignKey(SalesPerson, on_delete=models.PROTECT, related_name="sales")
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT, related_name="sales")
    price = models.PositiveIntegerField()

    def get_api_url(self):
        return reverse("api_list_sales")